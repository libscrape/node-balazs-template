const log = require('npmlog');
const Sequelize = require('sequelize');

const con = new Sequelize({
    dialect: 'sqlite',
    storage: './main.sqlite',
    logging: mesg => {
        log.verbose('db', mesg);
    }
});

module.exports.Post = con.define('post', {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    title: Sequelize.STRING,
    content: Sequelize.TEXT,
});

module.exports.setup = async () => {
    await con.authenticate();
    await exports.Post.sync();
    log.info('db', 'Connection established to DB.');
};