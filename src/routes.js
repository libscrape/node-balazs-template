const router = require('express').Router();
const db = require('./db');

module.exports = () => {
    router.get('/ajax/user', (_, res, next) => {
        db.Post.findAll().then(res.send).catch(next);
    });

    router.post('/ajax/user', (req, res, next) => {
        db.Post.create({ title: req.body.title, content: req.body.content })
            .then(res.send).error(next);
    });

    router.delete('/ajax/user/:id', (req, res) => {
        db.Post.findByPk(req.params.id).then(post => post.destroy()).error(next);
    });
    return router;
};