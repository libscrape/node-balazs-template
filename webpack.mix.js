const mix = require('laravel-mix');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

mix.setPublicPath('public');
mix.webpackConfig({
    plugins: [
        new VueLoaderPlugin()
    ]
});
mix.js('assets/main.js', './')
    .sass('assets/main.scss', './');