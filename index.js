const log = require('npmlog');
const express = require('express');
const app = express();

const db = require('./src/db');

log.info('boot', 'Application starting up...');
db.setup().then(() => {
    app.use((req, res, next) => {
        log.verbose('http', '%s %s\n UA: %s\n IP: %s', req.method, req.path, req.headers['user-agent'], req.ip);
    });
    app.use(require('body-parser').json());
    app.use('/', express.static('./public'));
    app.use('/ajax', require('./src/routes')());
    app.use((err, req, res, next) => {
        log.warn('frontend', 'Error:');
        log.warn('frontend', err);
        res.status(500).send({ error: err.message });
    });
    app.listen(3000, () => {
        log.info('boot', 'Frontend ready to roll!');
    });
}).catch(e => {
    log.error('db', 'Unable to connect to db. This program will now exit.');
    process.exit(1);
})